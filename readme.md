# Dataface


![datavis](http://bonjourmonde.net/bonjourmonde/user/pages/21.Hybridations-variables/images/thumb.jpg)    

_dataFace_ is an experimental tool for experimental typography. It would be describe more as  post-production app, more close to a toy then a software.

### Tiptoeing into history

It's been a while that fonts are encoded, they are not anymore drawn but describe as a set of instructions (THE VECTOR).
It means that there is an abstraction layer between what I see and the computation behind it. This gap is fertile, it already been use by
[Letteror](http://letterror.com/) in the Beowolf font, ramdomly-shaped generated font. Their aims was to bring back error, little mistake in shapes that would make
cold computing more lifely. They use to see their font as a virus, mutating between each print. Amazing isn't it ?
As them, we would like to pursue a quest of noise. Dispite all the effort made to hide it for a smooth experience, digital shape got grain and resitance, sometime
it break if we push it to far.  

### Visibility readability debility
The software allow ones to play with the font data -the font itself- and disrupte the shape initialy draw.

## Getting Started

This project exist in two versions :   
* version 1 (v.1) is build with d3.js and fonttools
* version 2 (v.2) is build with python and use SVG font format (for now)

## Author
[Bonjour Monde](http://bonjourmonde.net)


## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
