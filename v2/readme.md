# dataface v2



## Getting Started

Here is some informations to help use this version of dataface.    

NB : this project is underconstruction, for now it mainly transform the font with the [magnet script]().

### Files description

```dataface.py``` is the main python script.    
``` convert.sh``` tansform the svg file into otf files using fontforge and the ```script.pe``` file.

### Prerequisites

You need to install python 3 and this library :    
* numpy
* svgpathtools
* lxml

The font that you want to work with must be in **svg format** (this will change)


## Running dataface v2

simply run
```bash
    python3 dataface.py myfont.svg
```



## To do

### main program :
* a better system for filtering svg Files
* import fontforge directly for converting font
* better implementation of precoded function / effect to use

### graphical interface interface
* everything


---
